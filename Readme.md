## Installation of dependencies ##

### system ###

(assuming Gentoo GNU/Linux with portage)

```sh
emerge --noreplace sys-apps/file dev-vcs/git dev-lang/go
```

### gems ###

```sh
bundle
```

## Set up environment variables ##

(assuming repository is located in `~/bin`, shell is ZSH)

in `~/.zshrc`:

```sh
eval "$(~/bin/bin init)"
```
