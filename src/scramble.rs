use std::env;
use std::fs::OpenOptions;
use std::os::unix::prelude::FileExt;

/// scramble bytes in a file, in-place
/// scramble again to restore file to its original state
/// not cryptographically secure at all

const KEY: [u8; 100] = [
    43, 153, 86, 137, 18, 237, 232, 65, 183, 153, 215, 114, 75, 67, 203, 102, 23, 234, 34, 243,
    129, 134, 123, 177, 133, 161, 90, 205, 212, 205, 245, 178, 188, 114, 131, 132, 85, 24, 246, 9,
    38, 168, 231, 9, 98, 73, 90, 30, 237, 58, 197, 174, 24, 31, 109, 152, 191, 231, 126, 156, 251,
    39, 130, 62, 170, 74, 33, 25, 115, 162, 214, 201, 14, 173, 88, 147, 30, 96, 132, 217, 194, 205,
    206, 141, 121, 126, 112, 253, 239, 119, 155, 91, 181, 248, 200, 132, 106, 213, 254, 179,
];

const BLOCK_SIZE: usize = KEY.len();

fn main() {
    let path = env::args().nth(1).unwrap();
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .open(path)
        .unwrap();

    let mut buffer = vec![0; BLOCK_SIZE * 10];
    let mut offset = 0;

    loop {
        buffer.resize(buffer.capacity(), 0);
        let bytes_read = file.read_at(&mut buffer, offset).unwrap();

        if bytes_read == 0 {
            break;
        }

        buffer.truncate(bytes_read);

        for (byte, key_byte) in buffer.iter_mut().zip(KEY.iter().cycle()) {
            *byte ^= key_byte
        }

        file.write_all_at(&buffer, offset).unwrap();

        offset += bytes_read as u64;
    }

    file.sync_all().unwrap();
}
