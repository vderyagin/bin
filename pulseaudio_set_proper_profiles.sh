#!/bin/sh

# contents of UDEV rule to use this:
#
# KERNEL=="card1" SUBSYSTEMS=="sound" ACTION=="remove" ENV{ID_MODEL}=="SMSL_M6" ENV{SOUND_INITIALIZED}=="1" RUN+="/home/vderyagin/.bin/pulseaudio_set_proper_profiles.sh"
#
# KERNEL=="card1" SUBSYSTEMS=="sound" ACTION=="change" ENV{ID_MODEL}=="SMSL_M6" ENV{SOUND_INITIALIZED}=="1" RUN+="/home/vderyagin/.bin/pulseaudio_set_proper_profiles.sh --delayed"

if [ "$1" = '--delayed' ]; then
  echo 'sleep 3 && /home/vderyagin/.bin/pulseaudio_set_proper_profiles.sh' | at now
  exit 0
fi

PULSE_SERVER=`ls /tmp/pulse-*/native | head -1`
USER=vderyagin

if [ -z "$PULSE_SERVER" ]; then
  exit 0
fi

run_pactl() {
  sudo -u $USER pactl --server $PULSE_SERVER $@
}

if `run_pactl list short cards | grep 'SMSL_M6' >/dev/null 2>&1`; then
  run_pactl set-card-profile 'alsa_card.usb-SMSL_SMSL_M6-00' 'output:iec958-stereo'
  run_pactl set-card-profile 'alsa_card.pci-0000_00_1f.3' 'off'
else
  run_pactl set-card-profile 'alsa_card.pci-0000_00_1f.3' 'output:analog-stereo+input:analog-stereo'
fi
